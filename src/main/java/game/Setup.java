package game;

import engine.Engine;
import engine.graphics.Window;
import engine.logic.GameLogic;

public class Setup {
    public static boolean VR_mode = false;

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            GameLogic gameLogic = new Game();
            Window.WindowOptions opts = new Window.WindowOptions();
            opts.cullFace = true;
            opts.showFps = true;
            opts.compatibleProfile = true;
            Engine gameEng = new Engine("Epic Test Game", vSync, opts, gameLogic);
            gameEng.run();
        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}