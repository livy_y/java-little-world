package engine.vr;

import org.lwjgl.openvr.OpenVR;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.Dictionary;
import java.util.Hashtable;

import static org.lwjgl.openvr.VR.*;
import static org.lwjgl.openvr.VRSystem.VRSystem_GetRecommendedRenderTargetSize;
import static org.lwjgl.openvr.VRSystem.VRSystem_GetStringTrackedDeviceProperty;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * A class for handling the VR headset.
 */
public class VR {
    public String e_symbol;
    public String e_descr;

    public String model = "null";
    public String serial = "null";
    public int recommended_width;
    public int recommended_height;

    /**
     *
     */
    public VR() {
        Dictionary<String, String> res = new Hashtable<String, String>();

        System.err.println("VR_IsRuntimeInstalled() = " + VR_IsRuntimeInstalled());
        System.err.println("VR_RuntimePath() = " + VR_RuntimePath());
        System.err.println("VR_IsHmdPresent() = " + VR_IsHmdPresent());

        try (MemoryStack stack = stackPush()) {
            IntBuffer peError = stack.mallocInt(1);

            int token = VR_InitInternal(peError, 0);
            if (peError.get(0) == 0) {
                try {
                    OpenVR.create(token);

                    model = VRSystem_GetStringTrackedDeviceProperty(
                            k_unTrackedDeviceIndex_Hmd,
                            ETrackedDeviceProperty_Prop_ModelNumber_String,
                            peError
                    );
                    serial = VRSystem_GetStringTrackedDeviceProperty(
                            k_unTrackedDeviceIndex_Hmd,
                            ETrackedDeviceProperty_Prop_SerialNumber_String,
                            peError
                    );

                    IntBuffer w = stack.mallocInt(1);
                    IntBuffer h = stack.mallocInt(1);
                    VRSystem_GetRecommendedRenderTargetSize(w, h);

                    recommended_width = w.get(0);
                    recommended_height = h.get(0);

                    System.err.println("Model Number : " + model);
                    System.err.println("Serial Number: " + serial);


                    System.err.println("Recommended Width : " + recommended_width);
                    System.err.println("Recommended Height: " + recommended_height);
                } finally {
                    VR_ShutdownInternal();
                }
            } else {
                e_symbol = VR_GetVRInitErrorAsSymbol(peError.get(0));
                e_descr = VR_GetVRInitErrorAsEnglishDescription(peError.get(0));
                System.out.println("INIT ERROR SYMBOL: " + e_symbol);
                System.out.println("INIT ERROR  DESCR: " + e_descr);
            }
        }
    }
}
