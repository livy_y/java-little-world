package engine.logic;

import engine.graphics.Window;
import engine.input.Mouse;

public interface GameLogic {

    void init(Window window) throws Exception;

    void input(Window window, Mouse mouseInput);

    void update(float interval, Mouse mouseInput, Window window);

    void render(Window window);

    void cleanup();
}
